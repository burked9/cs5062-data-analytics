# CS5062 - DATA ANALYTICS
I am using this Repo as the main area for all the Etivities and all the Quizes, as I go through this module. That will include all the datasets provided etc.

In Anaconda Navigator I am running an environment just for this module, and I am using Git-Gui in Warp Environment on my Macbook in order to push everything to GitLab.


### Getting started - Additional Information

```bash
pip install foobar
```

## Usage

```python
import foobar # sample
```


## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/burked9/cs5062-data-analytics.git
git branch -M main
git push -uf origin main
```

## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.com/burked9/cs5062-data-analytics/-/settings/integrations)

## Collaborate with your team

- [ ] [Invite team members and collaborators](https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Set auto-merge](https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing (SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)

***

# Editing this README

Thanks to [makeareadme.com](https://www.makeareadme.com/) for this template.


## Name
CS Data Science - EDA, Exploratory Data Analysis

## Description
This is mostly EDA - but it is important and on file for any other work I will be doing for my coding portfolio.


## Visuals
Depending on what you are making, it can be a good idea to include screenshots or even a video (you'll frequently see GIFs rather than actual videos). Tools like ttygif can help, but check out Asciinema for a more sophisticated method.



## Usage
This is for my module work only, but anyone can use it.

## Support
Anyone who is reading this can message me by email burked9@gmail.com or get me on my mobile, Oh8Seven1933409

## Roadmap
I may combine all or some of these Etivities into a summary notebook when the module is over.

## Contributing
If anyone wants to contact me and contribute feel free.


## License
For open source projects, say how it is licensed.

[MIT](https://choosealicense.com/licenses/mit/)

## Project status
